/**********************************************************************
        Copyright 2014 Shannon Mackey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**********************************************************************/

#include <QString>
#include <QtTest>

#include <QDebug>
#include <QFile>

#include "idfx.h"


class LibIDFxTest : public QObject
{
    Q_OBJECT

public:
    LibIDFxTest();
    ~LibIDFxTest();

private:
    IDFx *_IDFx;
//    IDFFile _IDFfile;
//    QFile _IDDfile;
//    QFile _IDDxfile;
    QStringList _idf_test_files;


private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();
    void datafile_readIDF();
    void idfxt_readIDF();
    void idfxt_readIDFx();
    void idfxt_writeIDF();
    void idfxt_writeIDFx();
};

LibIDFxTest::LibIDFxTest():
    _IDFx(new IDFx())
{
    _idf_test_files << "UNIT_TESTa.idf" ;
}

LibIDFxTest::~LibIDFxTest()
{
    delete _IDFx;
}

//! Put files in place for remaining tests.
/*! File names that are added to _test_files (in the constructor, from the list
 *  in the qrc resources) are extracted and written to filesystem for remaining tests.
 *  Now it attempts write without checking and will "fail" when files remain from
 *  earlier runs.  "Properly," one might ensure that the writes are successful,
 *  then delete them in the cleanupTestCase().  That is currently left
 *  as an exercise for the reader.
 */
void LibIDFxTest::initTestCase()
{
    foreach (QString file_string, _idf_test_files)
    {
        QFile::copy(QString(":/IDF/resources/%1").arg(file_string),
                             QString("%1").arg(file_string));
    }
}

void LibIDFxTest::cleanupTestCase()
{

}

void LibIDFxTest::datafile_readIDF()
{

}

//! \test readIDF() \todo
// TODO: properly use test/todo doxygen flags, and non-header file function docs
void LibIDFxTest::idfxt_readIDF()
{
    //foreach .idf file found in this path, do:
    _IDFx->readIDF(_idf_test_files.at(0));
//    QList<DataObject*> objects = _IDFx->getIDFxObjects("");
//    QVERIFY(objects.count() == 727);
}

void LibIDFxTest::idfxt_readIDFx()
{
    QVERIFY2(true, "Failure"); //TODO: implement idfxt_readIDFx
}

void LibIDFxTest::idfxt_writeIDF()
{
    QVERIFY2(true, "Failure"); //TODO: implement idfxt_writeIDF
}

void LibIDFxTest::idfxt_writeIDFx()
{
//    _IDFx->writeIDFx();
    QVERIFY2(true, "Failure"); //TODO: implement idfxt_writeIDFx
}

QTEST_APPLESS_MAIN(LibIDFxTest)

#include "tst_libidfxtest.moc"
