#/**********************************************************************
#        Copyright 2014 Shannon Mackey

#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at

#        http://www.apache.org/licenses/LICENSE-2.0

#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#**********************************************************************/


QT       += testlib

QT       -= gui

TARGET = tst_libidfxtest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += tst_libidfxtest.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

RESOURCES += \
    resources.qrc

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libIDFx/release/ -llibIDFx
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libIDFx/debug/ -llibIDFx
else:unix: LIBS += -L$$OUT_PWD/../libIDFx/ -llibIDFx

INCLUDEPATH += $$PWD/../libIDFx
DEPENDPATH += $$PWD/../libIDFx
