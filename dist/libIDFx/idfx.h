/**********************************************************************
        Copyright 2014 Shannon Mackey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**********************************************************************/

#ifndef IDFX_H
#define IDFX_H

#include "libidfx_global.h"


//! IDFx
/*! A dynamically linkable C++ shared library
 * used by idfxt (command line tool for translating IDF files
 * to IDFx, and XML schema validating IDFx files.
 */

class LIBIDFXSHARED_EXPORT IDFx
{
public:
    IDFx();
    ~IDFx();

    /*! idfxt commandline interface functions:
     * these methods are exposed
     * for the commandline interface
     */

    //! Read in old format IDF file for automatic translation and validation
    bool readIDF(QString file_name);

    //! Read in new XML format IDFx file for schema validation
    bool readIDFx(QString file_name);

    //! Write out a valid IDFx format file
    bool writeIDFx(QString file_name);


    //! Get an explanation of error
    /*! Any read, write or validation will return false if the operation
     * failed to complete successfully.  Call getError() after any of these
     * to see what may have happened.
     */
    QString getError();

    //! Single handle to the the QObject derived object model
    /*! Using this single pointer to the parent-most QObject-based
     * model object, you have convenient ability to get any/all
     * child IDFx Objects and their IDFx Fields with QObject's methods.
     * such as:
     *
     *  - QList<T> QObject::findChildren(const QString & name = QString(),
     *                               Qt::FindChildOptions options =
     *                               Qt::FindChildrenRecursively) const
     * This method returns a list of DataObjects that have names matching
     * a QString or QRegExp, and this can be done recursively.
     * "name" corresponds to "label" as used in the IDFx schema; it may be blank.
     *  http://qt-project.org/doc/qt-5/qobject.html#findChildren
     *
     *  - QList<QByteArray> QObject::dynamicPropertyNames() const
     * This will return all of the IDFx Field names of a particular IDFx Object.
     *  http://qt-project.org/doc/qt-5/qobject.html#dynamicPropertyNames
     *
     *  - QVariant QObject::property(const char * name) const
     * This returns the value for a particular IDFx Field one a particular
     * IDFx Object.  Use Qt's 'foreach' to get values of all IDFx Fields.
     *  http://qt-project.org/doc/qt-5/qobject.html#property
     *  http://qt-project.org/doc/qt-5/containers.html#the-foreach-keyword
     */
//    DataObject* getRootObject();

    //! Get the IDFx Type.
    /*! Convenience method to get the IDFx Object Type of a particular DataObject.
     */
    QString getIDFxObjectType();

    //! Get IDFx ObjectTypes from DataModel
    /*! Convenience method to return a QStringList of distinct IDFx Types
     * of instances in this particular file,
     * as opposed to all possible IDFx Types in the schema.
     */
//    QSet<QString> getIDFxObjectTypes();

    //! Get IDFx DataObjects from DataModel
    /*! Convenience method to return a QList of pointers to DataObject instances
     * of a particular IDFx Type.  In conjunction with getIDFxObjectTypes(),
     * this is useful for batch processing Objects by Type.  When called with no
     * parameters, this will return all DataObjects.  Alternately, a QRegExp
     * can be used to specify only a substring of the Type.
     */
//    QList<DataObject*> getIDFxObjects(QString object_type = "");
//    QList<DataObject*> getIDFxObjects(QRegExp object_type);

    //! Get IDFx Fields
    /*! Convenience method returning a QHash of IDFx Fields and their values.
     * A QString value will be the Key, and a QVariant compatible object
     * will be the value.  All Keys of a given QHash can be gotten as a
     * QStringList with QHash::Keys(), similar to QList<QByteArray> returned by
     * QObject::dynamicPropertyNames().
     * In conjunction with getIDFxObjects(), this is useful for batch processing
     * IDFx Fields in IDFx Objects.
     */
//    QHash<QString, QVariant> getIDFxFields(DataObject* object_instance);

    //! Convenience method to get IDFx schema version of the model
    QString getIDFxSchemaVersion();


private:

    //! Validate an IDFx file in memory
    /*! This is always called once the file is loaded/converted into the IDFx
     */
    bool validate();

};

#endif // IDFX_H
