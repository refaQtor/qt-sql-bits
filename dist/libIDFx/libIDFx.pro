#/**********************************************************************
#        Copyright 2014 Shannon Mackey

#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at

#        http://www.apache.org/licenses/LICENSE-2.0

#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#**********************************************************************/

QMAKE_CXXFLAGS += -std=c++11
QT       += sql xmlpatterns

QT       -= gui

TARGET = libIDFx
TEMPLATE = lib

DEFINES += LIBIDFX_LIBRARY

SOURCES += idfx.cpp

HEADERS += idfx.h\
        libidfx_global.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

OTHER_FILES +=

RESOURCES += \
    rsrc.qrc


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../external/libEPDF/bin/Debug/release/ -lEPDF
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../external/libEPDF/bin/Debug/debug/ -lEPDF
else:unix: LIBS += -L$$PWD/../external/libEPDF/bin/Debug/ -lEPDF

INCLUDEPATH += $$PWD/../external/libEPDF
DEPENDPATH += $$PWD/../external/libEPDF
