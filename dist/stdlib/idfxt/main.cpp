#include <iostream>

#include "epData.hpp"

using namespace std;

const string verstring = "0.1";

/**
 * @brief import/export EnergyPlus data files
 * @param argc
 * @param argv
 * @return 0 on success
 */
int main(int argc, char **argv)
{
    cout << "idfxt - version: " << verstring << endl;
	
	epData *Data = new epData();
	
	
	delete Data;
    return 0;
}


//usage: idfxt --import "filename"
//usage: idfxt --[re]set "filename"
//usage: idfxt --insert "[parent:]object" "property1:value1" "property2:value2a|value2b|value2c"
//usage: idfxt --