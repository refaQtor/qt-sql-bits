#ifndef EPDATA_HPP
#define EPDATA_HPP

#include "sqlite3.h"

/*namespace energyplus
{*/

/**
 * @class epObject
 * @author Shannon Mackey
 * @date 09/25/14
 * @file epData.hpp
 * @brief 
 */
class epObject
{
public:
	/**
	 * @brief 
	 * @return 
	 */
	epObject()
	{
	}
	~epObject()
	{
	}

};



/**
 * @class epData
 * @author Shannon Mackey
 * @date 09/25/14
 * @file epData.hpp
 * @brief 
 */
class epData
{

public:
	/**
	 * @brief 
	 * @return 
	 */
	epData()
	{
		DB = new sqlite3();
		sqlite3_open("filename", &DB);
        sqlite3_close(DB);
	}
	~epData()
	{
	}

private:
	sqlite3 *DB;

	
};

/*} //energyplus namespace*/


#endif // EPDATA_HPP
