/**********************************************************************
        Copyright 2014 Shannon Mackey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**********************************************************************/

#include <QCoreApplication>
#include <QCommandLineParser>
#include <QFile>
#include <QDebug>

#include <sstream>

#include "epdf.h"


// VERSION STRINGS
// set these manually in preparation for release
 QString MAJOR = "0";
 QString MINOR = "1";
//a git commit identifier is appended at the end for the -version switch
//the git id is updated when doing a make/clean
//so, do a clean rebuild just prior to preparing the final executable to distribute
//this scheme requires the a Custom Make Step
//  "git describe --always > idfxt/vcs.txt"
// (unfortunately, this ends up in the .pro.user file, and
//  therefore, each developer needs to add it to their setup)
//TODO: switch to purely .pro solution

QString getVersionString()
{
    QFile vcs(":/version/resources/vcs.txt");
    QString commit = "error";
    if (vcs.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QString testline = vcs.readLine();
        commit = (testline.contains(QRegExp("[0-9a-f]{7}",Qt::CaseInsensitive)))
                ? commit = testline
                : "invalid";
        vcs.close();
    }
    return QString("%1.%2-%3")
            .arg(MAJOR)
            .arg(MINOR)
            .arg(commit)
            .remove("\n");
}

void prepareCommandLineParser(QCommandLineParser *parser)
{
    parser->setApplicationDescription("\nThe IDFx Tool (idfxt) provides two services:\n"
                                     " -Import non-xml (version 8.2) format IDF and produce a valid IDFx version of file.\n"
                                     " -Validate IDFx files against the IDFx Schema.\n"
                                     "   idfxt [ -i IDF input file | -x IDFx input file ] [ -o outputfile ]\n"
                                     "   idfxt [ -v | -h ]");

    QCommandLineOption optionInput("i",
                                   "specifies IDF input file, implying the desire for a conversion from IDF to a validated IDFx."
                                   "Validation results write to the terminal.",
                                   "IDF-file");
    parser->addOption(optionInput);

    QCommandLineOption optionValidate("x",
                                      "specifies IDFx input file, implying the desire to validate the input file. "
                                      "Validation results write to the terminal.",
                                      "IDFx-file");
    parser->addOption(optionValidate);

    QCommandLineOption optionOutput("o",
                                    "specifies an output file for the results of previous -t(ransform) switch, if any. "
                                    "If no -o switch is specified any results will dump to the terminal.",
                                    "output-file");
    parser->addOption(optionOutput);

    parser->addHelpOption(); //h
    parser->addVersionOption(); //v
}

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    //explicit setup for QSettings
    QCoreApplication::setApplicationName("idfxt");
    QCoreApplication::setApplicationVersion(getVersionString());
    QCoreApplication::setOrganizationName("EnergyPlus");
    QCoreApplication::setOrganizationDomain("nrel.gov");

    //process arguments
    QCommandLineParser *parsedArgs = new QCommandLineParser();
    prepareCommandLineParser(parsedArgs);
    parsedArgs->process(app);

    std::stringstream sql_data;
    sql_data << "CREATE TABLE one";
    epData extlibdatafile();


//no event loop    return app.exec();
}

//TODO: idfxt handle import IDF file
//TODO: idfxt handle import and validate resultant IDFx against IDDx schema
//TODO: idfxt handle read IDFx and validate against IDDx schema

//TODO: -i	: specifies IDF input file,
//TODO: -x	: specifies IDFx input file,
//TODO: -t 	: specifies XSLT transform file
//TODO: -o	: specifies an output file for the results
